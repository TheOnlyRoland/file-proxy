#!/usr/bin/env bash

docker run -it --rm \
    -v "$(pwd)":/strm \
    -v "$(pwd)/.m2":/root/.m2 \
    -w /strm \
    maven:3.6.0-jdk-8-alpine mvn clean package -Dmaven.test.skip=true

docker build -t strm-proxy:1.0 .



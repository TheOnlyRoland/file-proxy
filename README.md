# File Proxy Service

## Запуск в dev-mode

0. В [/etc/hosts]() необходимо добавить IP-адреса и hostname'ы машин Samba в соответствие с таблицей проксирования

1. Собрать docker образ: ```docker build -t strm-proxy:1.0 .```

2. Запустить docker-compose в проекте: ```docker-compose -f docker-compose-dev.yml pull && docker-compose -f docker-compose-dev.yml up -d```

3. Так как подразумевается, что это production решение, таблица для проксирования в MSSQL отсутствует и при старте 
приложение упадет. Для ее инициализации нужно выполнить юнит-тест в директории [/test/java/ru/kryptonite/proxy/ProxyTest.java](./src/test/java/ru/kryptonite/proxy/ProxyTest.java)

4. Затем можно запускать докер контейнер сервиса: ```docker-compose up```

5. Для остановки тестовых сервисов samba и mssql с очисткой volume: ```docker-compose -f docker-compose-dev.yml down -v```

6. Для остановки proxy-сервиса: ```docker-compose down -v```

---

Обращаться к сервису нужно следующим образом:
```bash
curl -X GET <hostname>:8080/rest/file/5CE50A37C6ADAF00,1
```

## Конфигурация docker-compose файла

Необходимо заполнить два раздела в compose-файле:
1. **environment**
    - *MSSQL_URL* - URL-адрес машины (\<hostname>(\<ipadress>)\\\\\<instance>:\<port>), на которой развернута БД с таблицей для проксирования
    - *MSSQL_USER*, *MSSQL_PASSWORD* - имя пользователя и пароль для аутентификации на MSSQL сервере
    - *SAMBA_USER*, *SAMBA_PASSWORD* - имя пользователя и пароль для авторизации на SMB-серверах
    - *SAMBA_DOMAIN* - имя SMB домена
    
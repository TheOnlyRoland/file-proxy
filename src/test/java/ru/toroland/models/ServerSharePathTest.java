package ru.toroland.models;

import org.junit.Test;
import static org.junit.Assert.*;

public class ServerSharePathTest {

    @Test
    public void toStringTest() {

        String cluster = "0198";
        String partition = "016A";
        String fileName = "5CFF316A9F996B00";

        String path = new ServerSharePath(fileName).toString();

        assertEquals(cluster + "/" + partition + "/" + fileName + ".strm", path);

    }

}

package ru.toroland;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.toroland.models.DataSource;
import ru.toroland.services.PathService;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Unit test only for initialization of demo environment
 */
@RunWith(JUnit4.class)
public class ProxyInitTest {

    Logger log = LoggerFactory.getLogger(ProxyInitTest.class);

    @BeforeClass
    public static void init() throws SQLException {

        SQLServerDataSource database = new SQLServerDataSource();
        database.setURL("jdbc:sqlserver://localhost\\\\MSSQLSERVER:1433");
        database.setUser("sa");
        database.setPassword("<MySQLAdmin>");
        database.setDatabaseName("master");

        Map<Integer, DataSource> testDataSources = new HashMap<>();
        testDataSources.put(1, new DataSource(
                "name1",
                "127.0.0.1:10445",
                "\\\\server1\\streams",
                1,
                "",
                0)
        );
        testDataSources.put(2, new DataSource(
                "name2",
                "127.0.0.1:10446",
                "\\\\server2\\streams",
                1,
                "",
                0)
        );
        testDataSources.put(3, new DataSource(
                "name3",
                "127.0.0.1:445",
                "\\\\windows1\\DevShare",
                1,
                "",
                0)
        );

        Connection connection = database.getConnection();
        Statement statement = connection.createStatement();

        statement.execute(
                "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='T_DATASERVICES' and xtype='U')" +
                        "CREATE TABLE [dbo].[T_DATASERVICES] (\n" +
                        "   [ServiceId] [int] NOT NULL,\n" +
                        "   [Name] [varchar](50) NOT NULL DEFAULT (''),\n" +
                        "   [Address] [varchar](50) NOT NULL DEFAULT (''),\n" +
                        "   [StorageName] [varchar](50) NOT NULL DEFAULT (''),\n" +
                        "   [ServiceType] [int] NOT NULL DEFAULT ((0)),\n" +
                        "   [DeleteSessionsConditions] [varchar](128) NOT NULL DEFAULT (''),\n" +
                        "   [AreaId] [tinyint] NOT NULL DEFAULT ((0))\n" +
                        ") ON [PRIMARY]"
        );

        String firstInsertQuery = "INSERT INTO [dbo].[T_DATASERVICES]\n" +
                "   VALUES (1, " +
                "'" + testDataSources.get(1).getName() + "', " +
                "'" + testDataSources.get(1).getAddress() + "', " +
                "'" + testDataSources.get(1).getStorageName() + "', " +
                testDataSources.get(1).getServiceType() + ", " +
                "'" + testDataSources.get(1).getDeleteSessionsConditions() + "', " +
                testDataSources.get(1).getAreaId() + ")";

        statement.execute(firstInsertQuery);

        String secondInsertQuery = "INSERT INTO [dbo].[T_DATASERVICES]\n" +
                "   VALUES (2, " +
                "'" + testDataSources.get(2).getName() + "', " +
                "'" + testDataSources.get(2).getAddress() + "', " +
                "'" + testDataSources.get(2).getStorageName() + "', " +
                testDataSources.get(2).getServiceType() + ", " +
                "'" + testDataSources.get(2).getDeleteSessionsConditions() + "', " +
                testDataSources.get(2).getAreaId() + ")";

        statement.execute(secondInsertQuery);

        String thirdInsertQuery = "INSERT INTO [dbo].[T_DATASERVICES]\n" +
                "   VALUES (3, " +
                "'" + testDataSources.get(3).getName() + "', " +
                "'" + testDataSources.get(3).getAddress() + "', " +
                "'" + testDataSources.get(3).getStorageName() + "', " +
                testDataSources.get(3).getServiceType() + ", " +
                "'" + testDataSources.get(3).getDeleteSessionsConditions() + "', " +
                testDataSources.get(3).getAreaId() + ")";

        statement.execute(thirdInsertQuery);

        connection.close();

    }

    @Test
    public void getDataSourceTest() throws SQLException {

//        PathService pathService = new PathService(
//                "localhost\\\\MSSQLSERVER:1433",
//                "sa",
//                "<MySQLAdmin>",
//                "master"
//        );
//        pathService.dataInit();
//
//        DataSource dataSource = pathService.getDataSource("1");
//        assertEquals(dataSource.getStorageName(), "\\\\server1\\streams");
//        log.debug("path: " + dataSource.getStorageName());
//
//        assertEquals(dataSource.getServerName(), "server1");
//        log.debug("server: " + dataSource.getServerName());
//
//        assertEquals(dataSource.getShareName(), "streams");
//        log.debug("share: " + dataSource.getShareName());

    }


}
package ru.toroland.services.file;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.beans.factory.annotation.Autowired;
import ru.toroland.exceptions.BadGatewayException;
import ru.toroland.exceptions.InternalServerException;
import ru.toroland.models.DataSource;
import ru.toroland.services.FileService;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;

@RunWith(JUnit4.class)
public class FileServiceTest {

    @Autowired
    FileService fileService;

    /**
     * Should throw exception on a non-existing file
     *
     * @throws IOException
     */
    @Test(expected = InternalServerException.class)
    public void nonExistingFile() throws IOException {

        DataSource dataSource = new DataSource(
                "name1",
                "127.0.0.1:10445",
                "server1\\streams",
                1,
                "",
                0);

        FileOutputStream outputStream = new FileOutputStream(FileDescriptor.out);
        fileService.getFile("5CE50A37C6ADAF04", dataSource, outputStream);

    }

    /**
     * Should throw exception on incorrect credentials
     *
     * @throws IOException
     */
    @Test(expected = InternalServerException.class)
    public void incorrectCredentials() throws IOException {

        DataSource dataSource = new DataSource(
                "name1",
                "127.0.0.1:10445",
                "server1\\streams",
                1,
                "",
                0);

        FileOutputStream outputStream = new FileOutputStream(FileDescriptor.out);
        fileService.getFile("5CE50A37C6ADAF00", dataSource, outputStream);

    }

    /**
     * Should fail on unknown hostname
     *
     * @throws IOException
     */
    @Test(expected = BadGatewayException.class)
    public void hostNotFound() throws IOException {

        DataSource dataSource = new DataSource(
                "name1",
                "127.0.0.1:10445",
                "server404\\streams",
                1,
                "",
                0);

        FileOutputStream outputStream = new FileOutputStream(FileDescriptor.out);
        fileService.getFile("5CE50A37C6ADAF00", dataSource, outputStream);

    }

    /**
     *
     * Should get file from Windows SMB source
     *
     */
    @Test
//    @Ignore
    public void getFileFromWindows() throws IOException {

        DataSource dataSource = new DataSource(
                "name1",
                "127.0.0.1:445",
                "windows1\\DevShare",
                1,
                "",
                0);

        FileOutputStream outputStream = new FileOutputStream(FileDescriptor.out);
        fileService.getFile("5CE50A37C6ADAF00", dataSource, outputStream);

    }



}

package ru.toroland.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.toroland.exceptions.BadRequestException;
import ru.toroland.exceptions.ResourceNotFoundException;
import ru.toroland.models.DataSource;
import ru.toroland.services.FileService;
import ru.toroland.services.PathService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@RestController
@RequestMapping("/rest/file")
public class FileController {

    private static Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    PathService pathService;

    @Autowired
    FileService fileService;

    @RequestMapping(value = "/{pointer}", method = RequestMethod.GET)
    public void download(@PathVariable String pointer, HttpServletResponse response) {

        if (!pointer.isEmpty()) {
            List<String> reqContent = Arrays.asList(pointer.split(","));
            if (reqContent.size() > 1) {

                String fileName = reqContent.get(0);
                Set<DataSource> dataSources = reqContent
                        .subList(1, reqContent.size())
                        .stream()
                        .flatMap(id -> Stream.of(pathService.getDataSource(id)))
                        .filter(source -> source != null)
                        .collect(Collectors.toSet());

                if (dataSources.isEmpty()) {
                    throw new BadRequestException("Empty response. Data resources not found");
                }

                dataSources.forEach(dataSource -> {
                    try {
                        logger.info("Accessing to " + dataSource.getStorageName() + ". File name: " + fileName);
                        fileService.getFile(fileName, dataSource, response.getOutputStream());
                    } catch (IOException e) {
                        throw new ResourceNotFoundException("File not accessible or not found. " + e.getMessage());
                    }
                });

            } else {
                throw new BadRequestException("Invalid server identifiers");
            }
        } else {
            throw new BadRequestException("Request is empty");
        }
    }

}

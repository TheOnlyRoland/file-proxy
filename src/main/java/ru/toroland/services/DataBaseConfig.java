package ru.toroland.services;


import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.toroland.models.DataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class DataBaseConfig {

    private static Logger logger = LoggerFactory.getLogger(DataBaseConfig.class);

    @Value("${mssql.url:localhost\\\\MSSQLSERVER:1433}")
    String url;
    @Value("${mssql.user:sa}")
    String user;
    @Value("${mssql.password:<MysQLAdmin>}")
    String password;
    @Value("${mssql.db:''}")
    String name;

    @Profile("demo")
    @Bean("database")
    public SQLServerDataSource getDevDataSource() throws SQLException {
        SQLServerDataSource database = new SQLServerDataSource();
        database.setURL("jdbc:sqlserver://localhost:1433");
        database.setUser("sa");
        database.setPassword("1Secure*Password1");
        database.setDatabaseName("");
        initDatabase(database);
        return database;
    }


    @Profile("!demo")
    @Bean("database")
    public SQLServerDataSource getProdDataSource() {
        SQLServerDataSource database = new SQLServerDataSource();
        database.setURL("jdbc:sqlserver://" + url);
        database.setUser(user);
        database.setPassword(password);
        database.setDatabaseName(name);
        return database;
    }


    private void initDatabase(SQLServerDataSource database) throws SQLException {

        Map<Integer, DataSource> testDataSources = new HashMap<>();
        testDataSources.put(1, new DataSource(
                "name1",
                "127.0.0.1:10445",
                "\\\\server1\\streams",
                1,
                "",
                0)
        );
        testDataSources.put(2, new DataSource(
                "name2",
                "127.0.0.1:10445",
                "\\\\server2\\streams",
                1,
                "",
                0)
        );
        testDataSources.put(3, new DataSource(
                "name3",
                "127.0.0.1:10445",
                "\\\\windows1\\DevShare",
                1,
                "",
                0)
        );

        Connection connection = database.getConnection();
        Statement statement = connection.createStatement();

        statement.execute(
                "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='T_DATASERVICES' and xtype='U')" +
                        "CREATE TABLE [dbo].[T_DATASERVICES] (\n" +
                        "   [ServiceId] [int] NOT NULL,\n" +
                        "   [Name] [varchar](50) NOT NULL DEFAULT (''),\n" +
                        "   [Address] [varchar](50) NOT NULL DEFAULT (''),\n" +
                        "   [StorageName] [varchar](50) NOT NULL DEFAULT (''),\n" +
                        "   [ServiceType] [int] NOT NULL DEFAULT ((0)),\n" +
                        "   [DeleteSessionsConditions] [varchar](128) NOT NULL DEFAULT (''),\n" +
                        "   [AreaId] [tinyint] NOT NULL DEFAULT ((0))\n" +
                        ") ON [PRIMARY]"
        );


        statement.execute("TRUNCATE TABLE [dbo].[T_DATASERVICES] ");

        String firstInsertQuery = "INSERT INTO [dbo].[T_DATASERVICES]\n" +
                "   VALUES (1, " +
                "'" + testDataSources.get(1).getName() + "', " +
                "'" + testDataSources.get(1).getAddress() + "', " +
                "'" + testDataSources.get(1).getStorageName() + "', " +
                testDataSources.get(1).getServiceType() + ", " +
                "'" + testDataSources.get(1).getDeleteSessionsConditions() + "', " +
                testDataSources.get(1).getAreaId() + ")";

        statement.execute(firstInsertQuery);

        String secondInsertQuery = "INSERT INTO [dbo].[T_DATASERVICES]\n" +
                "   VALUES (2, " +
                "'" + testDataSources.get(2).getName() + "', " +
                "'" + testDataSources.get(2).getAddress() + "', " +
                "'" + testDataSources.get(2).getStorageName() + "', " +
                testDataSources.get(2).getServiceType() + ", " +
                "'" + testDataSources.get(2).getDeleteSessionsConditions() + "', " +
                testDataSources.get(2).getAreaId() + ")";

        statement.execute(secondInsertQuery);

        String thirdInsertQuery = "INSERT INTO [dbo].[T_DATASERVICES]\n" +
                "   VALUES (3, " +
                "'" + testDataSources.get(3).getName() + "', " +
                "'" + testDataSources.get(3).getAddress() + "', " +
                "'" + testDataSources.get(3).getStorageName() + "', " +
                testDataSources.get(3).getServiceType() + ", " +
                "'" + testDataSources.get(3).getDeleteSessionsConditions() + "', " +
                testDataSources.get(3).getAreaId() + ")";

        statement.execute(thirdInsertQuery);

        connection.commit();
        connection.close();


    }


}

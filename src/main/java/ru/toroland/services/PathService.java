package ru.toroland.services;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.toroland.exceptions.ResourceNotFoundException;
import ru.toroland.models.DataSource;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Proxy service
 */
@Service
public class PathService {

    private static Logger logger = LoggerFactory.getLogger(PathService.class);

    private ConcurrentHashMap<String, DataSource> dataSources = new ConcurrentHashMap<>();

    SQLServerDataSource database;

    @Autowired
    public PathService(@Autowired @Qualifier("database") SQLServerDataSource database) {
        this.database = database;
    }

    /**
     * Proxy data initialization
     *
     * @throws SQLException
     */
    @PostConstruct
    public void dataInit() throws SQLException {
        Connection connection = this.database.getConnection();
        Statement statement = connection.createStatement();

        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM [dbo].[T_DATASERVICES]");

            while (rs.next()) {
                DataSource dataSource = new DataSource(
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7)
                );

                String key = String.valueOf(rs.getInt(1));
                logger.info("Uploaded data sources: " + dataSource.toString());
                dataSources.put(key, dataSource);
            }
        } catch (SQLServerException e) {
            throw new ResourceNotFoundException("Proxying table is not found or connection is lost. " + e.getMessage());
        }

        connection.close();

    }

    public DataSource getDataSource(String id) throws NoSuchElementException {

        DataSource ds = dataSources.get(id);
        if (ds != null) {
            return ds;
        } else {
            throw new NoSuchElementException("Data source " + id + " not found");
        }

    }

}

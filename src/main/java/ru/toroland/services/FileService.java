package ru.toroland.services;

import com.hierynomus.smbj.session.Session;
import com.hierynomus.smbj.share.DiskShare;
import com.hierynomus.smbj.share.File;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.toroland.exceptions.BadGatewayException;
import ru.toroland.models.DataSource;
import ru.toroland.models.FileAccessConfig;
import ru.toroland.models.ServerSharePath;
import ru.toroland.pooling.SMBSessionPool;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.UnresolvedAddressException;

@Service
public class FileService {

    private static Logger logger = LoggerFactory.getLogger(FileService.class);

    @Autowired
    private SMBSessionPool connectionPool;

    public void getFile(String fileName, DataSource dataSource, OutputStream outputStream) throws IOException {

        String serverName = dataSource.getServerName();
        String shareName = dataSource.getShareName();
        Integer port = dataSource.getPort();

        Session session = connectionPool.getSession(serverName, port);
        logger.info("Accessing to share: " + dataSource.getStorageName());

        try (DiskShare share = (DiskShare) session.connectShare(shareName)) {

            String partialPath = new ServerSharePath(fileName).toString();
            FileAccessConfig fileConfig = new FileAccessConfig();
            fileConfig.setSubpath(partialPath);
            logger.info("Trying to get access to file: " + fileConfig.getSubpath());

            File file = share
                    .openFile(
                            fileConfig.getSubpath(),
                            fileConfig.getAccessMask(),
                            fileConfig.getFileAttributes(),
                            fileConfig.getSmb2ShareAccesses(),
                            fileConfig.getCreateDisposition(),
                            fileConfig.getCreateOptions()
                    );

            IOUtils.copy(file.getInputStream(), outputStream);
            connectionPool.releaseSession(serverName, port, session);

        } catch (UnresolvedAddressException addrException) {

            logger.info("Failed to connect to SMB share ", addrException);
            throw new BadGatewayException("Invalid SMB hostname or password. " + addrException.getMessage());
        }

    }

}

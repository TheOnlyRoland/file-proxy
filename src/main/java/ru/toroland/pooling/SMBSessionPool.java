package ru.toroland.pooling;

import com.hierynomus.security.SecurityProvider;
import com.hierynomus.smbj.SMBClient;
import com.hierynomus.smbj.SmbConfig;
import com.hierynomus.smbj.auth.AuthenticationContext;
import com.hierynomus.smbj.connection.Connection;
import com.hierynomus.smbj.session.Session;
import com.hierynomus.smbj.transport.tcp.async.AsyncDirectTcpTransportFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.toroland.exceptions.BadGatewayException;
import ru.toroland.services.FileService;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.channels.UnresolvedAddressException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SMBSessionPool {

    private static Logger logger = LoggerFactory.getLogger(SMBSessionPool.class);

    private static int POOL_SIZE = 10;

    private String user;
    private String password;
    private String domain;
    private SMBClient client;

    private Map<String, Session> sessionPool = new ConcurrentHashMap<>(POOL_SIZE);
    private Map<String, Session> usedSessions = new ConcurrentHashMap<>();

    @Autowired
    public SMBSessionPool(
            @Value("${samba.username:smbuser}") String user,
            @Value("${samba.password:''}") String password,
            @Value("${samba.domain:''}") String domain
    ) {
        this.user = user;
        this.password = password;
        this.domain = domain;
        SmbConfig smbConfig = SmbConfig
                .builder()
                .withTransportLayerFactory(new AsyncDirectTcpTransportFactory<>())
                .withMultiProtocolNegotiate(true)
                .build();
        this.client = new SMBClient(smbConfig);
        logger.debug(this.toString());
    }

    @PreDestroy
    private void destroy() throws IOException {
        sessionPool.forEach((server, session) -> {
            try {
                session.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        usedSessions.forEach((server, session) -> {
            try {
                session.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public Session getSession(String server, Integer port) throws IOException {
        try {
            Session session = sessionPool.remove(server);
            if (session == null || (session != null && !session.getConnection().isConnected())) {
                session = createSession(server, port);
            }
            usedSessions.put(getSessionKey(server, port), session);
            return session;
        } catch (NullPointerException e) {
            logger.debug("Connection to " + server + " is lost");
            throw e;
        }
    }

    public boolean releaseSession(String server, Integer port, Session session) {
        sessionPool.put(getSessionKey(server, port), session);
        return usedSessions.remove(server, session);
    }

    private Session createSession(String server, Integer port) throws IOException {

        logger.debug("Creating session to " + server + ", port " + port);
        try {
            Connection connection = client.connect(server, port);
            AuthenticationContext ac = new AuthenticationContext(user, password.toCharArray(), domain);
            return connection.authenticate(ac);
        } catch (UnresolvedAddressException addrException) {
            logger.info("Failed to connect to SMB source ", addrException);
            throw new BadGatewayException("Invalid SMB hostname or password. " + addrException.getMessage());
        }

    }

    private String getSessionKey(String server, Integer port) {
        return server + ":" + port;
    }

}

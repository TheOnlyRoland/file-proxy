package ru.toroland.models;

import com.hierynomus.msdtyp.AccessMask;
import com.hierynomus.msfscc.FileAttributes;
import com.hierynomus.mssmb2.SMB2CreateDisposition;
import com.hierynomus.mssmb2.SMB2CreateOptions;
import com.hierynomus.mssmb2.SMB2ShareAccess;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Access mask for reading files from Samba sources
 */
public class FileAccessConfig {

    private String subpath;

    private Set<AccessMask> accessMask = new HashSet<>(Arrays.asList(AccessMask.GENERIC_READ));

    private Set<FileAttributes> fileAttributes = null;

    private Set<SMB2ShareAccess> smb2ShareAccesses = SMB2ShareAccess.ALL;

    private SMB2CreateDisposition createDisposition = SMB2CreateDisposition.FILE_OPEN;

    private Set<SMB2CreateOptions> createOptions = null;

    public String getSubpath() {
        return subpath;
    }

    public void setSubpath(String subpath) {
        this.subpath = subpath;
    }

    public Set<AccessMask> getAccessMask() {
        return accessMask;
    }

    public Set<FileAttributes> getFileAttributes() {
        return fileAttributes;
    }

    public Set<SMB2ShareAccess> getSmb2ShareAccesses() {
        return smb2ShareAccesses;
    }

    public SMB2CreateDisposition getCreateDisposition() {
        return createDisposition;
    }

    public Set<SMB2CreateOptions> getCreateOptions() {
        return createOptions;
    }


}

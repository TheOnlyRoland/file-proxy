package ru.toroland.models;


public class ServerSharePath {

    private String cluster;
    private String partition;
    private String fileName;

    public ServerSharePath(String fileName) {

        Integer step = 9;

        String raw = Long.toBinaryString(java.lang.Long.parseLong(fileName, 16) >> 32);
        Integer length = raw.length();

        try {
            Long keyLong = Long.parseLong(raw.substring(length - 2 * step, length - step), 2);
            Long partitionLong = Long.parseLong(raw.substring(length - step, length), 2);

            String keyString = Long.toString(keyLong, 16).toUpperCase();
            String partitionString = Long.toString(partitionLong, 16).toUpperCase();

            this.cluster = String.format("%4s", keyString).replace(' ', '0');
            this.partition = String.format("%4s", partitionString).replace(' ', '0');
            this.fileName = fileName;
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid content: " + fileName + "\n" + e.getMessage());
        }

    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder()
                .append(cluster)
                .append("/" + partition)
                .append("/" + fileName + ".strm");

        return str.toString();
    }

}

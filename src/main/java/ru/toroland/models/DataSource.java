package ru.toroland.models;

/**
 * Content of row in proxy table
 */
public class DataSource {

    private String name;
    private String address;
    private String storageName;
    private Integer serviceType;
    private String deleteSessionsConditions;
    private Integer areaId;

    public DataSource(
            String name,
            String address,
            String storageName,
            Integer serviceType,
            String deleteSessionsConditions,
            Integer areaId
    ) {
        this.name = name;
        this.address = address;
        this.storageName = storageName;
        this.serviceType = serviceType;
        this.deleteSessionsConditions = deleteSessionsConditions;
        this.areaId = areaId;
    }

    public String getServerName() {
        return this.storageName
                .split("\\\\")[2];
    }

    public String getShareName() {
        return this.storageName
                .split("\\\\")[3];
    }

    public String getStorageName() {
        return this.storageName;
    }

    public String getAddress() {
        return address;
    }

    public String getIpAddress() {

        return this.address.split(":")[0];

    }

    public Integer getPort() {

        return Integer.parseInt(this.getAddress().split(":")[1]);

    }

    public String getName() {
        return name;
    }

    public Integer getServiceType() {
        return serviceType;
    }

    public String getDeleteSessionsConditions() {
        return deleteSessionsConditions;
    }

    public Integer getAreaId() {
        return areaId;
    }

    @Override
    public String toString() {
        return name + ": " + address + ": " + storageName;
    }
}

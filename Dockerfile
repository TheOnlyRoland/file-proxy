FROM oracle/graalvm-ce:19.2.0.1

ENV JAVA_HOME=/usr/local/graalvm-ce-1.0.0-rc15

COPY ./target/strm-proxy-1.0-SNAPSHOT.jar /
RUN chmod 777 /strm-proxy-1.0-SNAPSHOT.jar

ENTRYPOINT /strm-proxy-1.0-SNAPSHOT.jar
